package com.wordpress.shibbirweb.sqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class MySqulite extends SQLiteOpenHelper {

    private static String DBNAME = "studentdb";
    private static String TABLENAME = "studentinfo";
    private static String ID = "_id";
    private static String STUDENTNAME = "studentname";
    private static String PHONE = "phoneno";
    private static int version = 1;



    String DROPTABLE = "DROP TABLE IF EXISTS "+TABLENAME;

    private static String TABLECREATE = "CREATE TABLE " +TABLENAME +" (" +
            "   "+ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "   "+STUDENTNAME+ " varchar (255),"+
            "   "+PHONE+ " varchar(255));";



    public MySqulite(Context context) {
        super(context, DBNAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLECREATE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(DROPTABLE);
        onCreate(db);

    }
    public  boolean insertData(String name, String phone){


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put ("studentname",name);
        contentValues.put("phoneno",phone);
        db.insert(TABLENAME,null,contentValues);
                return true;
    }

    public ArrayList <Model> getAllData(){

        ArrayList <Model> arrayList = new ArrayList<Model>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLENAME,null);
        cursor.moveToFirst();
        while (cursor.isAfterLast()==false ){

            arrayList.add(new Model(cursor.getString(cursor.getColumnIndex("studentname")),cursor.getString(cursor.getColumnIndex("phoneno"))));
            cursor.moveToNext();
             }



        return arrayList;

    }


}
